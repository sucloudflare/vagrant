 <h1>Guia de início rápido do Vagrant</h1>

<h2>Descrição</h2>
<p>Este guia fornece uma introdução rápida sobre como usar o Vagrant para criar e gerenciar ambientes de desenvolvimento. O Vagrant facilita a configuração, a construção e a manutenção de ambientes virtuais de maneira consistente.</p>

<h2>Requisitos</h2>
<ul>
        <li><a href="https://www.vagrantup.com/downloads">Vagrant</a></li>
        <li><a href="https://www.virtualbox.org/wiki/Downloads">VirtualBox</a> (ou outro provedor de virtualização suportado)</li>
 <  /ul>

<h2>Instalação</h2>    <p><strong>1. Instale o Vagrant:</strong></p>
  <p>Baixe e instale o Vagrant a partir do <a href="https://www.vagrantup.com/downloads">site oficial</a>.</p>
    <p><strong>2. Instale o VirtualBox:</strong></p>
        <p>Baixe e instale o VirtualBox a partir do <a href="https://www.virtualbox.org/wiki/Downloads">site oficial</a>.</p>

  <h2>Criando seu Primeiro Projeto com Vagrant</h2>
  <h3>1. Inicializar um Novo Projeto</h3>
 <p>Abra o terminal e navegue até o diretório onde deseja criar seu projeto. Execute o comando:</p>
<pre>vagrant init</pre>
<p>Isso criará um arquivo <code>Vagrantfile</code> no diretório atual.</p>    <h3>2. Configurar o <code>Vagrantfile</code></h3>
 <p>Edite o <code>Vagrantfile</code> gerado para definir a configuração da VM. Por exemplo, para usar uma box do Ubuntu:</p>
  <pre><code>Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
end</code></pre>    <h3>3. Iniciar a Máquina Virtual</h3>
<p>Para criar e configurar a VM conforme definido no <code>Vagrantfile</code>, execute:</p>
<pre>vagrant up</pre>

   <h3>4. Conectar-se à Máquina Virtual</h3>
<p>Para acessar a VM via SSH, use o comando:</p>
<pre>vagrant ssh</pre>    <h3>5. Pausar, Desligar e Destruir a Máquina Virtual</h3>
<p><strong>Pausar VM:</strong></p>
<pre>vagrant suspend</pre>
<p><strong>Desligar a VM:</strong></p>
<pre>vagrant halt</pre>
<p><strong>Destruir a VM:</strong></p>
<pre>vagrant destroy</pre>

<h2>Comandos Importantes do Vagrant</h2>
 <p><strong>Inicializar um Novo Projeto:</strong></p>
<pre>vagrant init</pre>
<p><strong>Iniciar a Máquina Virtual:</strong></p>
<pre>vagrant up</pre>
<p><strong>Conectar-se via SSH:</strong></p>
<pre>vagrant ssh</pre>
<p><strong>Pausar a Máquina Virtual:</strong></p>
<pre>vagrant suspend</pre>
<p><strong>Desligar a Máquina Virtual:</strong></p>
<pre>vagrant halt</pre>
<p><strong>Destruir a Máquina Virtual:</strong></p>
<pre>vagrant destroy</pre>
<p><strong>Verificar o Status da Máquina Virtual:</strong></p>
<pre>vagrant status</pre>
<p><strong>Recarregar a Máquina Virtual (para aplicar mudanças no <code>Vagrantfile</code>):</strong></p>
<pre>vagrant reload</pre>
<p><strong>Listar Boxes Instaladas:</strong></p>
<pre>vagrant box list</pre>
<p><strong>Adicionar uma Nova Box:</strong></p>
<pre>vagrant box add &lt;box_name&gt;</pre>
<p><strong>Remover uma Box:</strong></p>
<pre>vagrant box remove &lt;box_name&gt;</pre>

<h2>Conclusão</h2>
 <p>O Vagrant simplifica a criação e o gerenciamento de ambientes de desenvolvimento, permitindo que você configure rapidamente máquinas virtuais consistentes. Com os comandos básicos e o <code>Vagrantfile</code>, você pode automatizar e padronizar seu ambiente de desenvolvimento, facilitando a colaboração e a manutenção.</p>
 <p>Para mais informações e comandos avançados, consulte a <a href="https://www.vagrantup.com/docs">documentação oficial do Vagrant</a>.</p>
</body>